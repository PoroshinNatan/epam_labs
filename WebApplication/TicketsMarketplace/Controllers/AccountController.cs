﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using TicketsMarketplace.Models;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketsMarketplace.Controllers
{
    public class AccountController : Controller
    {
        CustomUserManager userManager;
        SignInManager<User> signInManager;
        public AccountController(CustomUserManager userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if(ModelState.IsValid)
            {
                User user = await userManager.FindByNameAsync(model.Login); 
                if(user!=null)
                {
                    if(await userManager.CheckPasswordAsync(user,model.Password))
                    {
                        await signInManager.SignInAsync(user, model.RememberMe);
                        Response.Cookies.Append(
                            CookieRequestCultureProvider.DefaultCookieName,
                            CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(user.Localization)),
                            new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
                            );
                        return Redirect("/Home/Events");
                    }
                }
            }
            return View();
        }
        [HttpGet]
        public  IActionResult Login(string returnUrl = null)
        {

            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return Redirect("/Home/Events");
        }
        [Authorize]
        public IActionResult UserProfile()
        {
            return View(userManager.FindByNameAsync(User.Identity.Name).Result);
        }
    }

}

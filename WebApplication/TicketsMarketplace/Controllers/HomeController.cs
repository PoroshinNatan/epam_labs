﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketsMarketplace.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using TicketsMarketplace.Models.DataAbstractions;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;
using TicketsMarketplace.Models.DataAbstractions.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;


namespace TicketsMarketplace.Controllers
{
    public class HomeController : Controller
    {
        IStorage _storage;

        public HomeController(IStorage storage)
        {
            _storage = storage;
        }
        // GET: /<controller>/
        
        public IActionResult Events()
        {
            return View(_storage.Events.GetAll());
        }
        
        
        [HttpGet]
        public IActionResult Tickets(int id)
        {
            ViewData["event"] = _storage.Events.Get(id);
            return View(_storage.GetAvailableTickets(id));
        }
        [Authorize]
        public IActionResult MyTickets()
        {
            return View();
        }
        [Authorize]
        [HttpGet]
        public IActionResult MyTicketsSelling()
        {
            return PartialView(_storage.GetSellingTicketsByUser(User.Identity.Name));
        }
        [Authorize]
        [HttpGet]
        public IActionResult MyTicketsSold()
        {
            return PartialView(_storage.GetSoldTicketsByUser(User.Identity.Name));
        }
        [Authorize]
        [HttpGet]
        public IActionResult MyTicketsWaitingConfirmation()
        {
            return PartialView(_storage.GetWaitingForConfirmationTicketsByUser(User.Identity.Name));
        }

        [Authorize]
        [HttpGet]
        public IActionResult MyOrders()
        {
            return View(_storage.Orders.GetOrdersByUser(User.Identity.Name));
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models.DataAbstractions.Interfaces
{
    public interface IVenueRepository:IRepository
    {
        IEnumerable<Venue> GetAll();
        Venue Get(int id);

        void Add(Venue item);
        void Remove(Venue item);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models.DataAbstractions.Interfaces
{
    public interface ITicketRepository:IRepository
    {
        IEnumerable<Ticket> GetAll();
        IEnumerable<Ticket> GetTicketsOnEvent(int eventId);
        IEnumerable<Ticket> GetUserTickets(string userName);
        Ticket Get(int id);

        void Add(Ticket item);
        void Remove(Ticket item);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models.DataAbstractions.Interfaces
{
    public interface IStorage
    {
        ICityRepository Cities { get; }
        ITicketRepository Tickets { get; }
        IEventRepository Events { get; }
        IOrderRepository Orders { get; }
        IVenueRepository Venues { get; }

        IEnumerable<Ticket> GetAvailableTickets(int eventId);
        IEnumerable<Ticket> GetSellingTicketsByUser(string userName);

        IEnumerable<Order> GetSoldTicketsByUser(string userName);
        IEnumerable<Order> GetWaitingForConfirmationTicketsByUser(string userName);
    }
}

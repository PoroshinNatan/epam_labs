﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models.DataAbstractions.Interfaces
{
    public interface IOrderRepository:IRepository
    {
        IEnumerable<Order> GetOrdersByUser(string userName);
        IEnumerable<Order> GetAll();
        Order Get(int id);

        void Add(Order item);
        void Remove(Order item);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models.DataAbstractions.Interfaces
{
    public interface ICityRepository:IRepository
    {
        IEnumerable<City> GetAll();
        City Get(int id);

        void Add(City item);
        void Remove(City item);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;
using TicketsMarketplace.Models.DataAbstractions.Repositories;

namespace TicketsMarketplace.Models.DataAbstractions
{
    public class Storage : IStorage
    {
        public Storage()
        {
            Cities = new CityRepository();
            Venues = new VenueRepository();
            Tickets = new TicketRepository();
            Orders = new OrderRepository();
            Events = new EventRepository();
        }

        public ICityRepository Cities { get; private set; }
        public ITicketRepository Tickets { get; private set; }
        public IEventRepository Events { get; private set; }
        public IOrderRepository Orders { get; private set; }
        public IVenueRepository Venues { get; private set; }

        public IEnumerable<Ticket> GetAvailableTickets(int eventId)
        {
            var allTicketsOnEvent = Tickets.GetTicketsOnEvent(eventId);
            var availableTickets = new List<Ticket>();
            foreach(var ticket in allTicketsOnEvent)
            {
                if (Orders.GetAll().ToList().Exists(order => order.Ticket.Id == ticket.Id && order.Status != Status.Confirmed))
                    availableTickets.Add(ticket);
            }
            return availableTickets;
        }

        public IEnumerable<Ticket> GetSellingTicketsByUser(string userName)
        {
            var userTickets = Tickets.GetUserTickets(userName);
            var sellingTickets = new List<Ticket>();
            foreach(var ticket in userTickets)
            {
                if (Orders.GetAll().ToList().Exists(order=>order.Id==ticket.Id && order.Status!=Status.Confirmed) || 
                    !Orders.GetAll().ToList().Exists(order => order.Id == ticket.Id))
                    sellingTickets.Add(ticket);
            }
            return sellingTickets;
        }

        public IEnumerable<Order> GetSoldTicketsByUser(string userName)
        {
            var userTickets = Tickets.GetUserTickets(userName);
            var soldTickets = new List<Order>();
            foreach (var ticket in userTickets)
            {
                if (Orders.GetAll().ToList().Exists(order => order.Ticket.Id == ticket.Id))
                {
                    soldTickets.InsertRange(soldTickets.Count, 
                        Orders.GetAll().ToList()
                        .FindAll(order => order.Ticket.Id == ticket.Id && order.Status == Status.Confirmed));
                }                   
            }
            return soldTickets;
        }

        public IEnumerable<Order> GetWaitingForConfirmationTicketsByUser(string userName)
        {
            var userTickets = Tickets.GetUserTickets(userName);
            var waitingConfirmationTickets = new List<Order>();
            foreach (var ticket in userTickets)
            {
                if (Orders.GetAll().ToList().Exists(order => order.Ticket.Id == ticket.Id))
                {
                    waitingConfirmationTickets.InsertRange(waitingConfirmationTickets.Count,
                        Orders.GetAll().ToList()
                        .FindAll(order => order.Ticket.Id == ticket.Id && order.Status != Status.Confirmed));
                }
            }
            return waitingConfirmationTickets;
        }
    }
}

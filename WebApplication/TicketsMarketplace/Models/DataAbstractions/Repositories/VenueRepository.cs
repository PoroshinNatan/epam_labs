﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;

namespace TicketsMarketplace.Models.DataAbstractions.Repositories
{
    public class VenueRepository :IVenueRepository
    {
        private static readonly List<Venue> _venues = new List<Venue>();

        public void Add(Venue item)
        {
            _venues.Add(item);
        }

        public Venue Get(int id)
        {
            return _venues.Find(x=>x.Id==id);
        }

        public IEnumerable<Venue> GetAll()
        {
            return _venues;
        }

        public void Remove(Venue item)
        {
            _venues.Remove(item);
        }
    }
}

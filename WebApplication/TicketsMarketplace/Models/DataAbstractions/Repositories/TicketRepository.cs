﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;

namespace TicketsMarketplace.Models.DataAbstractions.Repositories
{
    public class TicketRepository : ITicketRepository
    {
        private static readonly List<Ticket> _tickets = new List<Ticket>();

        public void Add(Ticket item)
        {
            _tickets.Add(item);
        }

        public Ticket Get(int id)
        {
            return _tickets.Find(x=>x.Id==id);
        }

        public IEnumerable<Ticket> GetAll()
        {
            return _tickets;
        }

        public IEnumerable<Ticket> GetUserTickets(string userName)
        {
            return (from ticket in _tickets
                    where ticket.Seller.UserName==userName
                    select ticket);
        }

        public IEnumerable<Ticket> GetTicketsOnEvent(int eventId)
        {
            return (from n in _tickets
                    where n.Event.Id==eventId
                    select n); 
        }

        public void Remove(Ticket item)
        {
            _tickets.Remove(item);
        }
    }
}

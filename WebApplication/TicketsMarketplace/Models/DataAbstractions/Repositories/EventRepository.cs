﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;

namespace TicketsMarketplace.Models.DataAbstractions.Repositories
{
    public class EventRepository : IEventRepository
    {
        private static readonly List<Event> _events=new List<Event>();

        public void Add(Event item)
        {
            _events.Add(item);
        }

        public Event Get(int id)
        {
            return _events.Find(x=>x.Id==id);
        }

        public IEnumerable<Event> GetAll()
        {
            return _events;
        }

        public void Remove(Event item)
        {
            _events.Remove(item);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;

namespace TicketsMarketplace.Models.DataAbstractions.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private static readonly List<Order> _orders = new List<Order>();

        public void Add(Order item)
        {
            _orders.Add(item);
        }

        public Order Get(int id)
        {
            return _orders.Find(x => x.Id == id);
        }

        public IEnumerable<Order> GetAll()
        {
            return _orders;
        }

        public IEnumerable<Order> GetOrdersByUser(string userName)
        {
            return (from order in _orders
                    where order.Buyer.UserName==userName
                    select order);
        }

        public void Remove(Order item)
        {
            _orders.Remove(item);
        }
    }
}

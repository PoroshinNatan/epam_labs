﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;

namespace TicketsMarketplace.Models.DataAbstractions.Repositories
{
    public class CityRepository : ICityRepository
    {
        private static readonly List<City> _cities = new List<City>();

        

        public void Add(City item)
        {
            _cities.Add(item);
        }

        public City Get(int id)
        {
            return _cities.Find(x=>x.Id==id);
        }

        public IEnumerable<City> GetAll()
        {
            return _cities;
        }

        public void Remove(City item)
        {
            _cities.Remove(item);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="Неверный логин")]
        [DataType(DataType.Text)]
        [MaxLength(256)]
        public string Login { get; set; }
        [Required(ErrorMessage ="Неверный пароль")]
        [DataType(DataType.Password)]
        [MaxLength(50)]
        public string Password { get; set; }

        public bool RememberMe { get; set;}

        public string ReturnUrl { get; set; }
    }
}

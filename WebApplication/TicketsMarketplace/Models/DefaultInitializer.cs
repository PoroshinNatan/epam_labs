﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TicketsMarketplace.Models.DataAbstractions;
using TicketsMarketplace.Models.DataAbstractions.Interfaces;
using TicketsMarketplace.Models.DataAbstractions.Repositories;

namespace TicketsMarketplace.Models
{
    public static class DefaultInitializer
    {
        ///<summary>
        ///Создает стандартные данные для Task 1. В будущем будет удален.
        ///</summary>
        public static void Initial(CustomUserManager userManager, RoleManager<Role> roleManager, IStorage repositoryStorage)
        {

            //добавление пользователей и ролей
            roleManager.CreateAsync(new Role("Administrator") { Id = "1" }).Wait();
            roleManager.CreateAsync(new Role("User") { Id = "2" }).Wait();
            userManager.CreateAsync(new User("Admin", "Admin") { Id = "1", Localization = new CultureInfo("ru-RU") }).Wait();
            userManager.CreateAsync(new User("User", "User") { Id = "2", Localization = new CultureInfo("en-US") }).Wait();
            userManager.CreateAsync(new User("Tom", "Tom") { Id = "3", Localization = new CultureInfo("be-BY") }).Wait();
            userManager.AddToRoleAsync(userManager.FindByIdAsync("1").Result, "User").Wait();
            userManager.AddToRoleAsync(userManager.FindByIdAsync("2").Result, "User").Wait();
            userManager.AddToRoleAsync(userManager.FindByIdAsync("3").Result, "Administrator").Wait();

            var Cities = repositoryStorage.Cities;
            var Orders = repositoryStorage.Orders;
            var Venues = repositoryStorage.Venues;
            var Tickets = repositoryStorage.Tickets;
            var Events = repositoryStorage.Events;
            
            //города
            Cities.Add(new City { Id = 1, Name = "Гомель" });
            Cities.Add(new City { Id = 2, Name = "Питер" });
            Cities.Add(new City { Id = 3, Name = "Нью-Йорк" });
            Cities.Add(new City { Id = 4, Name = "Киев" });
            Cities.Add(new City { Id = 5, Name = "Париж" });
            Cities.Add(new City { Id = 6, Name = "Лондон" });

            //места
            Venues.Add(new Venue { Id = 1, Name = "Кинотеатр \"Октябрь\"", City = Cities.Get(1), Address = "ул. Барыкина 44" });
            Venues.Add(new Venue { Id = 2, Name = "Церковь", City = Cities.Get(3), Address = "ул. Украинская 8" });
            Venues.Add(new Venue { Id = 3, Name = "Мост", City = Cities.Get(2), Address = "ул. Петровская 21" });
            Venues.Add(new Venue { Id = 4, Name = "Озеро", City = Cities.Get(2), Address = "ул. Васильевская 32" });
            Venues.Add(new Venue { Id = 5, Name = "Небоскреб", City = Cities.Get(3), Address = "ул. Джорджа 7" });
            Venues.Add(new Venue { Id = 6, Name = "Памятник", City = Cities.Get(3), Address = "ул. Луиса 21" });
            Venues.Add(new Venue { Id = 7, Name = "Автозаправка", City = Cities.Get(4), Address = "ул. Украинская 34" });
            Venues.Add(new Venue { Id = 8, Name = "Кирпичный завод", City = Cities.Get(4), Address = "ул. Заводская 12" });
            Venues.Add(new Venue { Id = 9, Name = "Ресторан", City = Cities.Get(5), Address = "ул. Французская 17" });
            Venues.Add(new Venue { Id = 10, Name = "Эйфелева башня", City = Cities.Get(6), Address = "ул. Неизвестная 4" });
            Venues.Add(new Venue { Id = 11, Name = "Памятник Нельсону", City = Cities.Get(6), Address = "ул. Британская 12" });
            Venues.Add(new Venue { Id = 12, Name = "Антикварный магазин", City = Cities.Get(6), Address = "ул. Английская 11" });

            //события
            Events.Add(new Event
            {
                Id = 1,
                Name = "Стойкий оловяный солдатик",
                Date = new DateTime(2017, 9, 6),
                Venue = Venues.Get(3),
                Banner = "/images/Events/Solder.jpg",
                Desctiption = "Описание стойкого оловяного солдатика"
            });
            Events.Add(new Event
            {
                Id = 2,
                Name = "Ромео и Джульетта",
                Date = new DateTime(2017, 9, 12),
                Venue = Venues.Get(11),
                Banner = "/images/Events/Romeo.jpg",
                Desctiption = "Описание Ромео и Джульетты"
            });
            Events.Add(new Event
            {
                Id = 3,
                Name = "Юнона и Авось",
                Date = new DateTime(2017, 9, 7),
                Venue = Venues.Get(2),
                Banner = "/images/Events/Yunona.jpg",
                Desctiption = "Описание Юноны и Авося"
            });
            Events.Add(new Event
            {
                Id = 4,
                Name = "Концерт группы \"Монгол Шуддан\"",
                Date = new DateTime(2017, 9, 16),
                Venue = Venues.Get(6),
                Banner = "/images/Events/Skelet.jpg",
                Desctiption = "Описание концерта группы \"Монгол Шуддан\""
            });
            Events.Add(new Event
            {
                Id = 5,
                Name = "В.Винокур и Театр Пародий",
                Date = new DateTime(2017, 9, 21),
                Venue = Venues.Get(9),
                Banner = "/images/Events/Vinokur.jpg",
                Desctiption = "Описание В.Винокура и Театра Пародий"
            });

            //билеты
            Tickets.Add(new Ticket { Id = 1, Event = Events.Get(4), Price = 15, Seller = userManager.FindByIdAsync("1").Result, Notes = "Some Text" });
            Tickets.Add(new Ticket { Id = 2, Event = Events.Get(2), Price = 23, Seller = userManager.FindByIdAsync("2").Result, Notes = "Различный текст заметки" });
            Tickets.Add(new Ticket { Id = 3, Event = Events.Get(3), Price = 23.23M, Seller = userManager.FindByIdAsync("3").Result, Notes = "" });
            Tickets.Add(new Ticket { Id = 4, Event = Events.Get(4), Price = 14, Seller = userManager.FindByIdAsync("1").Result, Notes = "Какой-то текст" });
            Tickets.Add(new Ticket { Id = 5, Event = Events.Get(5), Price = 12, Seller = userManager.FindByIdAsync("2").Result, Notes = "Каждый новый текст отличается" });
            Tickets.Add(new Ticket { Id = 6, Event = Events.Get(3), Price = 16, Seller = userManager.FindByIdAsync("3").Result, Notes = "Где-то больше, где-то меньше" });
            Tickets.Add(new Ticket { Id = 7, Event = Events.Get(2), Price = 25, Seller = userManager.FindByIdAsync("1").Result, Notes = "Где-то есть запятые и точки, где-то нету." });
            Tickets.Add(new Ticket { Id = 8, Event = Events.Get(3), Price = 35, Seller = userManager.FindByIdAsync("2").Result, Notes = "Пустая строка, текста тут нету" });
            Tickets.Add(new Ticket { Id = 9, Event = Events.Get(4), Price = 44, Seller = userManager.FindByIdAsync("3").Result, Notes = "English text" });
            Tickets.Add(new Ticket { Id = 10, Event = Events.Get(5), Price = 13.23M, Seller = userManager.FindByIdAsync("2").Result, Notes = "" });
            Tickets.Add(new Ticket { Id = 11, Event = Events.Get(2), Price = 25, Seller = userManager.FindByIdAsync("2").Result, Notes = "Все" });
            Tickets.Add(new Ticket { Id = 12, Event = Events.Get(3), Price = 25, Seller = userManager.FindByIdAsync("3").Result, Notes = "Лучшее место" });
            Tickets.Add(new Ticket { Id = 13, Event = Events.Get(2), Price = 13, Seller = userManager.FindByIdAsync("1").Result, Notes = "Цена соответствует позиции" });
            Tickets.Add(new Ticket { Id = 14, Event = Events.Get(4), Price = 16, Seller = userManager.FindByIdAsync("2").Result, Notes = "Среднее место" });
            Tickets.Add(new Ticket { Id = 15, Event = Events.Get(3), Price = 17, Seller = userManager.FindByIdAsync("2").Result, Notes = "Почему везде русский текст?" });
            Tickets.Add(new Ticket { Id = 16, Event = Events.Get(5), Price = 17, Seller = userManager.FindByIdAsync("1").Result, Notes = "" });
            Tickets.Add(new Ticket { Id = 17, Event = Events.Get(5), Price = 15, Seller = userManager.FindByIdAsync("1").Result, Notes = "" });
            Tickets.Add(new Ticket { Id = 18, Event = Events.Get(2), Price = 13, Seller = userManager.FindByIdAsync("3").Result, Notes = "Правильно!" });
            Tickets.Add(new Ticket { Id = 19, Event = Events.Get(5), Price = 14, Seller = userManager.FindByIdAsync("3").Result, Notes = "" });

            //заказы
            Orders.Add(new Order { Id = 1, Buyer = userManager.FindByIdAsync("1").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(1), TrackNo = "asd34x4" });
            Orders.Add(new Order { Id = 2, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(1), TrackNo = "xasdhg65" });
            Orders.Add(new Order { Id = 3, Buyer = userManager.FindByIdAsync("3").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(6), TrackNo = "asdasdvbv" });
            Orders.Add(new Order { Id = 4, Buyer = userManager.FindByIdAsync("3").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(1), TrackNo = "56dfXsd" });
            Orders.Add(new Order { Id = 5, Buyer = userManager.FindByIdAsync("3").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(16), TrackNo = "AzfdF43" });
            Orders.Add(new Order { Id = 6, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(12), TrackNo = "dsf56fds" });
            Orders.Add(new Order { Id = 7, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(16), TrackNo = "dsfsr-dfs" });
            Orders.Add(new Order { Id = 8, Buyer = userManager.FindByIdAsync("1").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(18), TrackNo = "asdvv" });
            Orders.Add(new Order { Id = 9, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(11), TrackNo = "zcxbbb" });
            Orders.Add(new Order { Id = 10, Buyer = userManager.FindByIdAsync("3").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(10), TrackNo = "jkgasdfsd" });
            Orders.Add(new Order { Id = 11, Buyer = userManager.FindByIdAsync("3").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(4), TrackNo = "avbasdfsg" });
            Orders.Add(new Order { Id = 12, Buyer = userManager.FindByIdAsync("1").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(13), TrackNo = "xcvsf" });
            Orders.Add(new Order { Id = 13, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(12), TrackNo = "fgdvbcv" });
            Orders.Add(new Order { Id = 14, Buyer = userManager.FindByIdAsync("1").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(11), TrackNo = "r567rtfhy" });
            Orders.Add(new Order { Id = 15, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(2), TrackNo = "890oiuw" });
            Orders.Add(new Order { Id = 16, Buyer = userManager.FindByIdAsync("1").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(14), TrackNo = "nbmzxcxzd45" });
            Orders.Add(new Order { Id = 17, Buyer = userManager.FindByIdAsync("3").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(7), TrackNo = "7nfgsfsr" });
            Orders.Add(new Order { Id = 18, Buyer = userManager.FindByIdAsync("1").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(7), TrackNo = "x435x454-45es" });
            Orders.Add(new Order { Id = 19, Buyer = userManager.FindByIdAsync("2").Result, Status = Status.WaitingForConfirmation, Ticket = Tickets.Get(8), TrackNo = "bnserw4" });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models
{
    public enum Status
    {
        WaitingForConfirmation = 1,
        Confirmed,
        Rejected
    }
}

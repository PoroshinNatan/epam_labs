﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models
{
    public class User:IdentityUser
    {
        public User(string login,string password)
        {
            UserName = login;
            PasswordHash = password;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public CultureInfo Localization { get; set; }
        public string Address { get; set; }
    }
}

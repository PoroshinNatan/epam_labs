﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Globalization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace TicketsMarketplace.Models
{
    public class CustomUserManager : UserManager<User>
    {
        public CustomUserManager(IUserStore<User> store,
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators, 
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            IServiceProvider services,
            ILogger<UserManager<User>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            
        }
        public override Task<IdentityResult> CreateAsync(User user)
        {
            Store.CreateAsync(user, CancellationToken);
            return Task.FromResult(IdentityResult.Success);
        }
        public override Task<User> FindByNameAsync(string userName)
        {
            return Store.FindByNameAsync(userName, CancellationToken);
        }
        public override Task<bool> CheckPasswordAsync(User user, string password)
        {
            if (String.Compare(user.PasswordHash, password) == 0)
                return Task.FromResult(true);
            else
                return Task.FromResult(false);
        }
        public override Task<IdentityResult> AddToRoleAsync(User user, string role)
        {
            var targetRole = CustomRoleStore.Roles.Find(x => x.Name == role);

            var identityUserRole = new IdentityUserRole<string>() { RoleId = targetRole.Id, UserId = user.Id };

            targetRole.Users.Add(identityUserRole);
            user.Roles.Add(identityUserRole);
            return Task.FromResult(IdentityResult.Success);
        }
       
    }
}

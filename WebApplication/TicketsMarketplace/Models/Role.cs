﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsMarketplace.Models
{
    public class Role : IdentityRole
    {
        public Role(string roleName) : base(roleName)
        {
        }
    }
}
